﻿#include <iostream>
#include <string>

int main()
{
	std::cout << "write something: ";
	std::string text;
	std::cin >> text;
	std::string out = "Your text: ";
	std::cout << out + text << "\n";

	std::string writing = "The length of your text: ";
	std::cout << writing;
	std::cout << text.length() << "\n";

	std::cout << "The first character of your text: ";
	std::cout << text.front() << "\n";

	std::cout << "The last character of your text: ";
	std::cout << text.back() << "\n";
}